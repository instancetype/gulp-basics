/**
 * Created by instancetype on 8/16/14.
 */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
const
  gulp = require('gulp')
, uglify = require('gulp-uglify')
, concat = require('gulp-concat')
, coffee = require('gulp-coffee')
, es = require('event-stream')

gulp.task('coffee', function() {
  return gulp.src('src/*.coffee')
    .pipe(coffee())
    .pipe(gulp.dest('src'))
})

gulp.task('scripts', function() {
  var javaScriptFromCoffeeScript = gulp.src('src/*.coffee')
    .pipe(coffee())

  var js = gulp.src('src/*.js')

  return es.merge(javaScriptFromCoffeeScript, js)
    .pipe(concat('all.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('dist'))
})

gulp.task('watch', function() {
  gulp.watch('src/*.{js,coffee}', ['scripts'])
})